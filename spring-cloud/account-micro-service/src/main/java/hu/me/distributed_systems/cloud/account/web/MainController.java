package hu.me.distributed_systems.cloud.account.web;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import hu.me.distributed_systems.common.Result;

@RestController
public class MainController {
    @GetMapping(value = "mennyi", produces = MediaType.APPLICATION_PROBLEM_JSON_VALUE)
	Result mennyi() {
        return new Result("Hello");
    }
}
