package hu.me.distributed_systems.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import hu.me.distributed_systems.cloud.controller.CloudMainController;


@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(useDefaultFilters=false)
public class DemoApplication {

	// Case insensitive: could also use: http://accounts-service
	public static final String ACCOUNTS_SERVICE_URL
			= "http://ACCOUNTS-SERVICE";

	@Bean
	@LoadBalanced
	RestTemplate restTemplate() {
		return new RestTemplate();
	}
	@Bean
	public CloudMainController mainController(RestTemplate restTemplate) {
		return new CloudMainController(restTemplate, ACCOUNTS_SERVICE_URL);  // plug in account-service
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
