package hu.me.distributed_systems.cloud.controller;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Eredmeny {
    private String eredmeny;
}
