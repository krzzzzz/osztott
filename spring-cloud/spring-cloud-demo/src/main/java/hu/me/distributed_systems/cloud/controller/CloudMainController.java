package hu.me.distributed_systems.cloud.controller;

import hu.me.distributed_systems.common.Result;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequiredArgsConstructor
public class CloudMainController {

    private final RestTemplate restTemplate;
    private final String dependencyUrl;


    @GetMapping(value = "mennyi", produces = MediaType.APPLICATION_PROBLEM_JSON_VALUE)
    Eredmeny miAzEletErtelme() {
        // meghivni a masik rest vegpontot

            Result res = restTemplate.getForObject(dependencyUrl
                    + "/mennyi", Result.class);
        return new Eredmeny("Elet ertelme " + res.getResult() );
    }
}
