package com.example.hexa.domain;

import lombok.Data;

@Data
public class Author {
    final String name;
}
