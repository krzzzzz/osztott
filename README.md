
Kiszerveztuk a kozos fuggoseget (Result osztalyt) egy hagyomanyos jar projektbe.
profibb megoldas
https://www.baeldung.com/spring-boot-dependency

tobb modula maven:
https://www.baeldung.com/maven-multi-module

docker:
https://www.baeldung.com/dockerizing-spring-boot-application


gcloud auth configure-docker
cp ../../cloud-server/target/cloud-server-0.0.1-SNAPSHOT.jar eureka.jar

docker build --file=Dockerfile --tag=eu.gcr.io/bitbot-static/eureka:2 --rm=true .
docker push eu.gcr.io/bitbot-static/eureka:1
gcloud app deploy --image-url=eu.gcr.io/bitbot-static/eureka:1 --version=1

docker build --file=Dockerfile.accMiSer --tag=eu.gcr.io/bitbot-static/account-micro-service:1  --rm=true .
docker push eu.gcr.io/bitbot-static/account-micro-service:1
gcloud app deploy --image-url=eu.gcr.io/bitbot-static/account-micro-service:1 --version=1

url-ek
- https://eureka-dot-bitbot-static.appspot.com
- https://account-micro-service-dot-bitbot-static.appspot.com

https://cloud.google.com/appengine/docs/flexible/custom-runtimes/managing-projects-apps-billing