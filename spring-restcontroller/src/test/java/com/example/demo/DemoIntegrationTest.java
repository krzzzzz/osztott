package com.example.demo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.reactive.function.client.WebClient;

import javax.servlet.ServletContext;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// MockitoJUnitRunner.class
// ha @Mock van
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { DemoApplication.class })
@WebAppConfiguration
public class DemoIntegrationTest {
    @Autowired
    private WebApplicationContext wac;

    @MockBean
    private Fuggoseg fuggoseg;

    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }


    @Test
    public void givenWac_whenServletContext_thenItProvidesProbaControler() {
        ServletContext servletContext = wac.getServletContext();

        Assert.assertNotNull(servletContext);
        Assert.assertTrue(servletContext instanceof MockServletContext);
        Assert.assertNotNull(wac.getBean("probaController"));
    }

    @Test
    public void givenSimaURI_whenMockMVC_thenVerifyResponse() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/sima"))
                .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.value").value("sima"))
                    .andReturn();

        assertEquals("application/json",
                mvcResult.getResponse().getContentType());
    }


    @Test
    public void givenFuggosegURI_whenMockMVC_thenVerifyResponse() throws Exception {
        final String mockResponse = "mockResponse";

        when(fuggoseg.getValue(anyString())).thenReturn(mockResponse);
        MvcResult mvcResult = this.mockMvc.perform(get("/fuggoseg"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.value").value(mockResponse))
                .andReturn();

        assertEquals("application/json",
                mvcResult.getResponse().getContentType());
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(fuggoseg, times(1)).getValue(argumentCaptor.capture());
        assertEquals("bemenet", argumentCaptor.getValue());

    }
    @Test
    public void givenIntErtekURI_whenMockMVC_thenVerifyResponse() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(
                get("/intErtek")
                        .param("input", "a"))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andReturn();
    }


    @MockBean
    MyWebClientBuilder mockWebClientBuilder;

    @Test
    public void WebClientMockTest() throws Exception {
        WebClient mockWC = mock(WebClient.class);
      when(mockWebClientBuilder.build(anyString())).thenReturn(mockWC);

        MvcResult mvcResult = this.mockMvc.perform(
                get("/tovabb"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn();
    }
}
