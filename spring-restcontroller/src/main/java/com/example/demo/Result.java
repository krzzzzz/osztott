package com.example.demo;

import lombok.Data;

@Data
public class Result {
    private final String value;
}
