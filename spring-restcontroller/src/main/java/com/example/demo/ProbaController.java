package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

@RestController
public class ProbaController {
    private Fuggoseg fuggoseg;

    @Autowired
    public void setFuggoseg(Fuggoseg fuggoseg) {
        this.fuggoseg = fuggoseg;
    }

    @GetMapping("/sima")
    Result sima() {
        return new Result("sima");
    }

    @GetMapping("/fuggoseg")
    Result getFuggoseg() {
        return new Result(this.fuggoseg.getValue("bemenet"));
    }


    @GetMapping("/intErtek")
    Result intErtek(@RequestParam("bemenet") Integer bemenet) {
        return new Result(this.fuggoseg.getValue(bemenet.toString()));
    }

    @Autowired
    public void setMyWebClientBuilder(MyWebClientBuilder myWebClientBuilder) {
        this.myWebClientBuilder = myWebClientBuilder;
    }

    MyWebClientBuilder myWebClientBuilder;

    @GetMapping("/tovabb")
    Result tovabb() {
        WebClient webclient = myWebClientBuilder.build("https://postman-echo.com/get");
        String response = webclient.get().exchange()
                .block()
                .bodyToMono(String.class)
                .block();
        System.out.println(response);
        return new Result(this.fuggoseg.getValue("megjott"));
    }

}