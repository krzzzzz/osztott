package com.example.demo;

import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class MyWebClientBuilder {
    public WebClient build(String url) {
        return WebClient.create(url);
    }
}
